/// Chinese Remainder Theorem
///
/// Returns the smallest number x such that:
/// x % a1 = r1,
/// x % a2 = r2,
/// ...
///
/// # Examples
/// ```
/// use utils::euclid::chinese_remainder_solver;
///
/// let a_ns = vec![(3, 5), (1, 7), (6, 8)];
/// assert_eq!(chinese_remainder_solver(a_ns), 78);
/// ```
pub fn chinese_remainder_solver(a_ns: Vec<(u128, u128)>) -> u128 {
    let n: u128 = a_ns.iter().map(|x| x.1).product();
    let mut sum = 0;

    for item in a_ns {
        let m = item.1;
        let z = n / m;
        let y = modinverse((z % m) as i128, m as i128).unwrap() as u128;
        let w = (y * z) % n;
        sum += (item.0 * w) % n;
    }
    sum % n
}

/// Extended Euclidean algorithm
///
/// Returns (gcd(a, b), x, y) such that ax + by = gcd(a, b)
///
/// # Examples
///
/// ```
/// use utils::euclid::egcd;
/// assert_eq!(egcd(3, 5), (1, 2, -1));
/// ```
pub fn egcd(a: i128, b: i128) -> (i128, i128, i128) {
    if a == 0 {
        (b, 0, 1)
    } else {
        let (g, x, y) = egcd(b % a, a);
        (g, y - (b / a) * x, x)
    }
}

/// Modular multiplicative inverse
/// Returns x such that ax = 1 (mod m)
///
/// # Examples
/// ```
/// use utils::euclid::modinverse;
///
/// assert_eq!(modinverse(3, 11), Some(4));
/// ```
pub fn modinverse(a: i128, m: i128) -> Option<i128> {
    let (g, x, _) = egcd(a, m);
    if g != 1 {
        None
    } else {
        Some((x % m + m) % m)
    }
}
