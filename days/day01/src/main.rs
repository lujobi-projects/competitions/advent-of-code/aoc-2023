use std::vec;

use utils::*;

const CURRENT_DAY: u8 = 1;

pub fn part1() -> i64 {
    let input = read_input!("\n", String);
    input
        .iter()
        .map(|line| {
            let nos = line
                .chars()
                .filter(|c| c.is_numeric())
                .map(|c| c.to_digit(10).unwrap() as i64)
                .collect::<Vec<i64>>();
            nos.first().unwrap() * 10 + nos.last().unwrap()
        })
        .sum()
}
pub fn part2() -> i64 {
    let input = read_input!("\n", String);
    let replacements: Vec<(String, String)> = vec![
        ("one".into(), "1".into()),
        ("two".into(), "2".into()),
        ("three".into(), "3".into()),
        ("four".into(), "4".into()),
        ("five".into(), "5".into()),
        ("six".into(), "6".into()),
        ("seven".into(), "7".into()),
        ("eight".into(), "8".into()),
        ("nine".into(), "9".into()),
    ];

    input
        .iter()
        .map(|line| {
            let mut line = line.clone();
            let first_findings = replacements
                .iter()
                .map(|(word, replacement)| {
                    (line.find(word).unwrap_or(line.len()), (word, replacement))
                })
                .min_by_key(|(idx, _)| *idx);
            let first_numeric_index = line.find(|c: char| c.is_numeric()).unwrap_or(line.len());
            if let Some((idx, replacement)) = first_findings {
                if idx < first_numeric_index {
                    line = line.replacen(replacement.0, replacement.1, 1);
                }
            }

            line = line.chars().rev().collect::<String>();
            let last_findings = replacements
                .iter()
                .map(|(word, replacement)| {
                    let a = word.chars().rev().collect::<String>();
                    (line.find(&a).unwrap_or(line.len()), (a, replacement))
                })
                .min_by_key(|(idx, _)| *idx);
            let last_numeric_index = line.find(|c: char| c.is_numeric()).unwrap_or(line.len());
            if let Some((idx, replacement)) = last_findings {
                if idx < last_numeric_index {
                    line = line.replacen(&replacement.0, replacement.1, 1);
                }
            }

            line = line.chars().rev().collect::<String>();

            let nos = line
                .chars()
                .filter(|c| c.is_numeric())
                .map(|c| c.to_digit(10).unwrap() as i64)
                .collect::<Vec<i64>>();

            nos.first().unwrap() * 10 + nos.last().unwrap()
        })
        .sum()
}

fn main() {
    print_answer!(CURRENT_DAY, 1, part1());
    print_answer!(CURRENT_DAY, 2, part2());
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part1() {
        assert_eq!(part1(), 54331);
    }

    #[test]
    fn test_part2() {
        assert_eq!(part2(), 54518);
    }
}
