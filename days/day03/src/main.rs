use itertools::Itertools;
use utils::*;

const CURRENT_DAY: u8 = 3;

pub fn is_surrounded_by_symbol(map: &Vec<String>, row: usize, col: usize, length: usize) -> bool {
    let left_offset = if col == 0 { 0 } else { 1 };
    let right_offset = if col + length == map[row].len() { 0 } else { 1 };

    if row != 0 {
        for i in (col - left_offset)..(col + length + right_offset) {
            if map[row - 1].chars().nth(i).unwrap() != '.' {
                return true;
            }
        }
    }
    if row != map.len() - 1 {
        for i in (col - left_offset)..(col + length + right_offset) {
            if map[row + 1].chars().nth(i).unwrap() != '.' {
                return true;
            }
        }
    }

    if col != 0 && map[row].chars().nth(col - 1).unwrap() != '.' {
        return true;
    }
    if col + length != map[row].len() && map[row].chars().nth(col + length).unwrap() != '.' {
        return true;
    }

    false
}

pub fn part1() -> i64 {
    let input = read_input!("\n", String);

    input
        .iter()
        .enumerate()
        .map(|(row, line)| {
            let mut start_idx = None;
            let mut sum = 0;
            let mut number = String::new();
            for (i, c) in line.chars().enumerate() {
                if c.is_numeric() {
                    if start_idx.is_none() {
                        start_idx = Some(i);
                    }
                    number.push(c);
                } else if !number.is_empty() {
                    if is_surrounded_by_symbol(&input, row, start_idx.unwrap(), number.len()) {
                        sum += number.parse::<i64>().unwrap();
                    }
                    number = String::new();
                    start_idx = None
                }
            }
            if !number.is_empty()
                && is_surrounded_by_symbol(&input, row, start_idx.unwrap(), number.len())
            {
                sum += number.parse::<i64>().unwrap();
            }
            sum
        })
        .sum()
}

pub fn part2() -> i64 {
    let input = read_input!("\n", String);

    let values_mapping = input
        .iter()
        .enumerate()
        .flat_map(|(row, line)| {
            let mut start_idx = None;
            let mut number = String::new();
            let mut numbers = vec![];
            for (i, c) in line.chars().enumerate() {
                if c.is_numeric() {
                    if start_idx.is_none() {
                        start_idx = Some(i);
                    }
                    number.push(c);
                } else if !number.is_empty() {
                    numbers.push((
                        number.parse::<i64>().unwrap(),
                        row,
                        start_idx.unwrap(),
                        number.len(),
                    ));
                    number = String::new();
                    start_idx = None
                }
            }
            if !number.is_empty() {
                numbers.push((
                    number.parse::<i64>().unwrap(),
                    row,
                    start_idx.unwrap(),
                    number.len(),
                ));
            }
            numbers
        })
        .collect::<Vec<_>>();

    let stars = input
        .iter()
        .enumerate()
        .flat_map(|(row, line)| {
            line.chars()
                .enumerate()
                .filter_map(|(i, c)| if c == '*' { Some((row, i)) } else { None })
                .collect::<Vec<_>>()
        })
        .collect::<Vec<_>>();

    stars
        .iter()
        .map(|star| {
            values_mapping
                .iter()
                .enumerate()
                .filter(|(_, (_, row, col, length))| {
                    let range = *col..(col + length);
                    if ((star.0 - 1)..=(star.0 + 1)).contains(row) {
                        return range.contains(&(star.1 - 1))
                            || range.contains(&(star.1))
                            || range.contains(&(star.1 + 1));
                    }
                    false
                })
                .unique_by(|(idx, _)| *idx)
                .map(|(_, (value, _, _, _))| value)
                .collect::<Vec<_>>()
        })
        .filter(|values| values.len() == 2)
        .map(|values| values[0] * values[1])
        .sum()
}

fn main() {
    print_answer!(CURRENT_DAY, 1, part1());
    print_answer!(CURRENT_DAY, 2, part2());
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part1() {
        assert_eq!(part1(), 521601);
    }

    #[test]
    fn test_part2() {
        assert_eq!(part2(), 80694070);
    }
}
