#[macro_use]
extern crate lalrpop_util;

lalrpop_mod!(#[allow(clippy::all)] pub parser); // synthesized by LALRPOP

use utils::*;

const CURRENT_DAY: u8 = 2;

pub type Games = Vec<Game>;

#[derive(Debug)]
pub struct Game {
    id: u64,
    rounds: Vec<Round>,
}
#[derive(Debug)]

pub enum ColorCount {
    Red(u64),
    Blue(u64),
    Green(u64),
}

#[derive(Debug)]
pub struct Round {
    red: u64,
    green: u64,
    blue: u64,
}

pub fn color_counts_to_round(color_counts: Vec<ColorCount>) -> Round {
    let mut red = 0;
    let mut green = 0;
    let mut blue = 0;
    for color_count in color_counts {
        match color_count {
            ColorCount::Red(count) => red += count,
            ColorCount::Green(count) => green += count,
            ColorCount::Blue(count) => blue += count,
        }
    }
    Round { red, green, blue }
}

pub fn part1() -> u64 {
    let input = parser::GamesParser::new().parse(&get_input!()).unwrap();

    let red_max = 12;
    let green_max = 13;
    let blue_max = 14;

    input
        .iter()
        .map(|game| {
            if game.rounds.iter().any(|round| {
                round.red > red_max || round.green > green_max || round.blue > blue_max
            }) {
                0
            } else {
                game.id
            }
        })
        .sum()
}

pub fn part2() -> u64 {
    let input = parser::GamesParser::new().parse(&get_input!()).unwrap();

    input
        .iter()
        .map(|game| {
            let red_max = game.rounds.iter().map(|round| round.red).max().unwrap();
            let green_max = game.rounds.iter().map(|round| round.green).max().unwrap();
            let blue_max = game.rounds.iter().map(|round| round.blue).max().unwrap();

            red_max * green_max * blue_max
        })
        .sum()
}

fn main() {
    print_answer!(CURRENT_DAY, 1, part1());
    print_answer!(CURRENT_DAY, 2, part2());
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part1() {
        assert_eq!(part1(), 2283);
    }

    #[test]
    fn test_part2() {
        assert_eq!(part2(), 78669);
    }
}
